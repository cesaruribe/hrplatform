from pydantic import BaseModel, constr
from typing import Optional
from datetime import datetime


class UserSchema(BaseModel):
    Apellido1: constr(max_length=20, regex="^[A-Z\s]*$") = "APELLIDOONE"
    Apellido2: constr(max_length=20, regex="^[A-Z\s]*$") = "APELLIDOTWO COMP"
    Nombre1: constr(max_length=20, regex="^[A-Z]*$") = "NOMBREONE"
    Nombre2: Optional[constr(max_length=20, regex="^[A-Z]*$")] = ""
    Pais: constr(max_length=20) = "COL"
    TipoDocumento: constr(max_length=20) = "CEDULA"
    Identificacion: constr(max_length=20) = "FK012345"
    email: constr(
        max_length=300,
        regex="^([A-Z]+).([A-Z0-9]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$",
    ) = "NOMBREONE.APELLIDO1@email.com"
    FechaCreacion: Optional[datetime] = None
    Area: str
    Estado: str = "Activo"
    FechaRegistro: datetime


class UserOut(BaseModel):
    Apellido1: str
    Apellido2: str
    Nombre1: str
    Nombre2: str
    Pais: str
    TipoDocumento: str
    Identificacion: str
    email: str
    FechaCreacion: datetime
    Area: str
    Estado: str = "Activo"
    FechaRegistro: datetime


class UserDB(UserOut):
    id: int