from app.api.models import UserSchema
from app.db import users, database


async def post(payload: UserSchema):
    query = users.insert().values(payload.dict())
    response = await database.execute(query=query)
    return response


async def get(id):
    query = users.select().where(id == users.c.id)
    return await database.fetch_one(query=query)


async def get_all():
    query = users.select()
    return await database.fetch_all(query=query)


async def put(id: int, payload: UserSchema):
    query = (
        users.update()
        .where(id == users.c.id)
        .values(payload.dict())
        .returning(users.c.id)
    )

    return await database.execute(query=query)


async def delete(id: int):
    query = users.delete().where(id == users.c.id)
    return await database.execute(query=query)