from typing import List


from app.api import crud
from app.api.models import UserDB, UserSchema
from fastapi import APIRouter, HTTPException, Path

router = APIRouter()


@router.post("/", response_model=UserDB, status_code=201)
async def create_user(payload: UserSchema):
    user_id = await crud.post(payload)

    response = {"id": user_id, **payload.dict()}
    return response


@router.get("/{id}/", response_model=UserDB)
async def read_user(
    id: int = Path(..., gt=0),
):
    user = await crud.get(id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@router.get("/", response_model=List[UserDB])
async def read_all_users():
    return await crud.get_all()


@router.put("/{id}/", response_model=UserDB)
async def update_user(
    payload: UserSchema,
    id: int = Path(..., gt=0),
):
    user = await crud.get(id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    user_id = await crud.put(id, payload)

    response = {"id": user_id, **payload.dict()}
    return response


@router.delete("/{id}/", response_model=UserDB)
async def delete_user(id: int):
    user = await crud.get(id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    await crud.delete(id)

    return user
