import os

from databases import Database
from sqlalchemy import Column, DateTime, String, Integer, Table, create_engine, MetaData
from sqlalchemy.sql import func


DATABASE_URL = "sqlite:///./users.db"

engine = create_engine(DATABASE_URL)
metadata = MetaData()
users = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("Apellido1", String(20)),
    Column("Apellido2", String(20)),
    Column("Nombre1", String(20)),
    Column("Nombre2", String(50), nullable=True),
    Column("Pais", String(20)),
    Column("TipoDocumento", String(20)),
    Column("Identificacion", String(20)),
    Column("email", String(300)),
    Column("FechaCreacion", DateTime, default=func.now(), nullable=False),
    Column("Area", String(300)),
    Column("Estado", String(300)),
    Column("FechaRegistro", DateTime, default=func.now()),
)


database = Database(DATABASE_URL)