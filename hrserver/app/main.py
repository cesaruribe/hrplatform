import secrets
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.middleware.cors import CORSMiddleware


from app.api import users, ping
from app.db import engine, metadata, database

metadata.create_all(engine)

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# security = HTTPBasic()


# def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
#     correct_username = secrets.compare_digest(credentials.username, "admin")
#     correct_password = secrets.compare_digest(credentials.password, "adminpwd")
#     print(correct_username, correct_password)
#     if not (correct_username and correct_password):
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Incorrect email or password",
#             headers={"WWW-Authenticate": "Basic"},
#         )
#     return credentials.username


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


# @app.get("/about")
# def read_current_user(username: str = Depends(get_current_username)):
#     return {"username": username}


app.include_router(ping.router)
app.include_router(users.router, prefix="/users", tags=["users"])
# app.include_router(
#     users.router, prefix="/users", tags=["users"], dependencies=[Depends(security)]
# )
