import json
from typing import List
import pytest

from app.api import crud


def test_create_user(test_app, monkeypatch):
    test_request_payload = {
        "Apellido1": "string",
        "Apellido2": "string",
        "Nombre1": "string",
        "Nombre2": "string",
        "Pais": "string",
        "TipoIdentidad": "string",
        "Identificacion": "string",
        "email": "string",
    }

    test_response_payload = {
        "id": 1,
        "Apellido1": "string",
        "Apellido2": "string",
        "Nombre1": "string",
        "Nombre2": "string",
        "Pais": "string",
        "TipoIdentidad": "string",
        "Identificacion": "string",
        "email": "string",
    }

    async def mock_post(payload):
        return 1

    monkeypatch.setattr(crud, "post", mock_post)

    response = test_app.post("/users/", data=json.dumps(test_request_payload))

    assert response.status_code == 201
    assert response.json() == test_response_payload


def test_create_user_invalid_json(test_app):
    response = test_app.post(
        "/users/",
        data=json.dumps(
            {
                "id": 1,
                "Apellido1": "string",
                "Apellido2": "string",
            }
        ),
    )
    assert response.status_code == 422


def test_read_user(test_app, monkeypatch):
    test_data = {
        "id": 1,
        "Apellido1": "string",
        "Apellido2": "string",
        "Nombre1": "string",
        "Nombre2": "string",
        "Pais": "string",
        "TipoIdentidad": "string",
        "Identificacion": "string",
        "email": "string",
    }

    async def mock_get(userid):
        return test_data

    monkeypatch.setattr(crud, "get", mock_get)

    response = test_app.get("/users/1")
    assert response.status_code == 200
    assert response.json() == test_data


def test_read_user_incorrect_id(test_app, monkeypatch):
    async def mock_get(userid):
        return None

    monkeypatch.setattr(crud, "get", mock_get)
    response = test_app.get("/users/10000")
    assert response.status_code == 404
    assert response.json()["detail"] == "User not found"


def test_read_all_users(test_app, monkeypatch):

    test_data = [
        {
            "id": 1,
            "Apellido1": "string",
            "Apellido2": "string",
            "Nombre1": "string",
            "Nombre2": "string",
            "Pais": "string",
            "TipoIdentidad": "string",
            "Identificacion": "string",
            "email": "string",
        },
        {
            "id": 2,
            "Apellido1": "string",
            "Apellido2": "string",
            "Nombre1": "string",
            "Nombre2": "string",
            "Pais": "string",
            "TipoIdentidad": "string",
            "Identificacion": "string",
            "email": "string",
        },
    ]

    async def mock_get_all():
        return test_data

    monkeypatch.setattr(crud, "get_all", mock_get_all)

    response = test_app.get("/users/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_update_user(test_app, monkeypatch):
    test_update_data = {
        "Apellido1": "other",
        "Apellido2": "string",
        "Nombre1": "string",
        "Nombre2": "string",
        "Pais": "string",
        "TipoIdentidad": "string",
        "Identificacion": "string",
        "email": "string",
        "id": 1,
    }

    async def mock_get(id):
        return True

    monkeypatch.setattr(crud, "get", mock_get)

    async def mock_put(id, payload):
        return 1

    monkeypatch.setattr(crud, "put", mock_put)

    response = test_app.put("/users/1/", data=json.dumps(test_update_data))
    print(response)

    assert response.status_code == 200
    assert response.json() == test_update_data


@pytest.mark.parametrize(
    "id, payload,status_code",
    [
        [1, {}, 422],
        [1, {"Apellido1": "otro"}, 422],
        [
            1000000,
            {
                "Apellido1": "other",
                "Apellido2": "string",
                "Nombre1": "string",
                "Nombre2": "string",
                "Pais": "string",
                "TipoIdentidad": "string",
                "Identificacion": "string",
                "email": "string",
                "id": 1,
            },
            404,
        ],
    ],
)
def test_update_user_invalid(test_app, monkeypatch, id, payload, status_code):
    async def mock_get(id):
        return None

    monkeypatch.setattr(crud, "get", mock_get)

    response = test_app.put(f"/users/{id}/", data=json.dumps(payload))
    assert response.status_code == status_code


def test_delete_user(test_app, monkeypatch):
    test_data = {
        "Apellido1": "other",
        "Apellido2": "string",
        "Nombre1": "string",
        "Nombre2": "string",
        "Pais": "string",
        "TipoIdentidad": "string",
        "Identificacion": "string",
        "email": "string",
        "id": 1,
    }

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(crud, "get", mock_get)

    async def mock_delete(id):
        return id

    monkeypatch.setattr(crud, "delete", mock_delete)
    response = test_app.delete("/users/1/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_delete_user_invalid_id(test_app, monkeypatch):
    async def mock_get(id):
        return None

    monkeypatch.setattr(crud, "get", mock_get)
    response = test_app.delete("/users/1000000/")
    assert response.status_code == 404
    assert response.json()["detail"] == "User not found"
