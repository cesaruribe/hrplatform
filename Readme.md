# HRsoft

A simple Human Resource software for employees management. The main goals of this applications are:

- Register users into the system and validate data inserted before save in database
- Read users registered into the system. These must be appear as _active_ or _inactive_
- Edit registered used. The read user view should have options to open user edition view

This web application was developed using **ReactJs** with **Material Ui** for the frontEnd and **FastAPI** for the backend

**NOTE**: This aplication is still in progress. Some features must be implemented yet and others could be improved. The styles used are the deafults defined by material ui. The styles has not be changed because a functional webapp wants to be constructed at the first iteration

## Features

### 1. Login view

This views was made to authenticate an admin user in order to allows him edition and insertion action.

![Login view](./img/Login.png)

**Annotations**

- At this moment, this authentication process don't have implemented communications with the backend
- The authentication in this moments occurs clicking the button login. This allows the user to get into the main page without credentials

### 2. Main Page

This views show the landing page for the application. It must be used as a presentational view to give at the user some information or a main dashboard

![Login view](./img/MainPage.png)

**Annotations**

- At this moment only contain basic information and it is used just for show its posible main use.
- It could show some general information, statistics dashboard or just presentational message - It view needs to be improve at the future.

### 3. Read Users

This view allows us to query for users to database. This system uses a SQlite database for easy use and migration but other DBMS could be used.

![Login view](./img/ViewUser.png)
![User view columns](./img/ViewUser_Columns.png)
![User view Filters Columns](./img/ViewUser_FiltersColumns.png)
![User view Filters operators](./img/ViewUser_FiltersOperators.png)
![User view Status](./img/ViewUser_Status.png)

**Annotations**

- The _Edit_ and _delete_ buttons not be full implemented. It still need connect the click action to redirect to the Edit view.
- The Edit view should has a filled forms with the existing user data after buttons _edit_ was clicked.

### 4. Edit/Insert Users

This view allows us insert or edit existing user in the system. This view must contain the following inputs

- **First Name**: In Uppercase without numbers and special characters
- **Second Name** [_Optional_]: In Uppercase without numbers and special characters
- **First Lastname**: In Uppercase without numbers and special characters
- **Second Lastname**: In Uppercase without numbers and special characters. Allows composite words
- **Country**: USA or COL
- **ID type**: CC, Foreign CC, Passport Number, ...
- **ID number**
- **Email** [_Automatic, not editable_]: `<FirstName>.<LastName><Number>@<email>.<text>`
- **Insertion Date**
- **Work Area**
- **Status**: Active, Inactive
- **Register Date**:

![Edit view](./img/EditUser.png)
![Edit view](./img/EditUser_Validations.png)

**Annotations**

- Not all fields has been implemented
- The click action in the button has not been implemented yet. This action must submit the form information and sended to the backend system.
- Some validations has been implemented in the frontend and the backend system

## Backend system

The backend was constructed using a simple model layer:

```bash
├── app
│   ├── api
│   │   ├── crud.py
│   │   ├── __init__.py
│   │   ├── models.py
│   │   ├── ping.py
│   │   └── users.py
│   ├── db.py
│   ├── __init__.py
│   ├── main.py
│   └── probe.py
├── base.sql
├── poetry.lock
├── pyproject.toml
├── tests
│   ├── conftest.py
│   ├── __init__.py
│   ├── test_ping.py
│   └── test_users.py
└── users.db
```

### Run the backend system

The backend has a file pyproject.toml which is a dependencies file constructed using the [Poetry](https://python-poetry.org/) dependency management and packaging.

#### 1. Installing dependencies

It is required at the first iteration, have installed poetry and/or using an python virtualenv management (pyenv was used in this case with python 3.7.7)

if you use [pyenv](https://github.com/pyenv/pyenv), you can run this fo install and activate this virtual enviroment:

```bash
$ pyenv install 3.7.7
$ pyenv local 3.7.7
```

After init or activate a python virtual enviroment (if you are using it)
you must run this to install the dependencies

```bash
$ poetry install
```

**Obs**: Poetry can freeze this .toml file and generate the file _requirements.txt_ by pip to install deppendencies, but thi file has not been generated yet.

#### 1. Start backend system

To start backend system, it is neccesary use the asgi _uvicorn_ beacuse FastAPI can work in asycn mode. Homewever, for avoid with this dependencies problems and have a scalable and more handable app, we can use a containerized technology as docker for encapsulate the app.

```bash
# it must be in hrserver folder
user@name:hrserver$ uvicorn app.main:app
```

## Frontend system

The frontend was develop with the ReactJs library and create-react-app tool. The structure is the following:

```
.
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md
└── src
    ├── App.jsx
    ├── App.test.js
    ├── components
    │   ├── Login
    │   ├── NavBar
    │   │   ├── Navbar.jsx
    │   │   └── Navbar.jsx~
    │   └── Users
    │       ├── AddUser.jsx
    │       └── AllUsers.jsx
    ├── hooks
    │   └── useToken.js
    ├── index.css
    └── index.js

```

The main information is located into the _src_ folder

#### 1. Start the frontend system

To start frontend system, It could be used npm or yarn

```bash
$ npm install
$ npm start
```

In the folder _hrclient_ there are more detailed instrutions.

## TO DO

- Implement a basic auth in the backend. Json web token is not required by he moment. It could be implementend in future iterations
- Implement the Login view to receive uername and password admin
- Implement the missing inputs in Edit view
- Implement an onclick action in button of Edit view to connect and send validated information to the backend
- Implement onclick action over butthon edit and delete in User view to edit existing user
- Fix some bug in dropdown ist in Edit user
- Use [Loguru](https://github.com/Delgan/loguru) to implement a log system in backend and integrate Loguru with uvicorn log handler.
