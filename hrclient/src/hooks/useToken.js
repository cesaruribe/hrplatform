import {useState} from 'react';

const useToken = () => {
    const getToken =  () => {
        const tokenStr = localStorage.getItem('token');
        const usrToken = JSON.parse(tokenStr)
        return usrToken?.token
    }
    
    const [token, setToken] = useState(getToken());
    
    const saveToken = (usrToken) => {
        localStorage.setItem('token',JSON.stringify(usrToken));
        setToken(usrToken.token)
    }
    
    return {
        setToken: saveToken,
        token
    }
}

export default useToken


