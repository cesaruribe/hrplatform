import {
    AppBar,
    Toolbar,
    Typography,
    makeStyles,
    IconButton,
    Button,
} from "@material-ui/core";
import React from "react";
import MenuIcon from "@material-ui/icons/Menu";
import { Link as RouterLink } from "react-router-dom";

const drawerWidth = 240;


const useStyles = makeStyles((theme) => ({
    menuButton: {
	marginRight: theme.spacing(2),
    },
    title: {
	flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
    link: {
	color: "#FFF",
    },
}));


const Navbar = (props) => {
    const classes = useStyles();
    return (
	<>
	    <AppBar position="fixed" color="primary">
		<Toolbar>
		    
		    <Typography className={classes.title} variant="h6">
			Aplicacion de Recursos Humanos
		    </Typography>
		    
		    <Button variant="text" color="inherit" component={RouterLink} to="/">
			Inicio
		    </Button>
		    
		    <Button
			variant="text"
			color="inherit"
			component={RouterLink}
			to="/users"
		    >
			usuarios
		    </Button>
		    
		    <Button
			variant="text"
			color="inherit"
			component={RouterLink}
			to="/users/edit"
		    >
			editar
		    </Button>

		    <Button variant="text" color="inherit" onClick={props.logout}>
			salir
		    </Button>

		</Toolbar>
	    </AppBar>
	</>
    );
};

export default Navbar;
