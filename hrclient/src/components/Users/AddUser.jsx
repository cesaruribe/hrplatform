import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import { Button, Typography } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles((theme) => ({
    container: { margin: theme.spacing(2) },
    margin: {
	margin: theme.spacing(2),
    },
}));

const optIdent = [
    { label: "Cedula", value: "CC" },
    { label: "Pasapote", value: "PP" },
    { label: "Cedula Extranjera", value: "CE" },
];

const AddUser = () => {
    const classes = useStyles();
    const [inputs, setInputs] = useState([
	{
	    id: "Nombre1",
	    label: "Primer Nombre",
	    placeholder: "JUAN",
	    value: "",
	    required: true,
	    error: false,
	    select: false,
	    helperText: "No debe llevar numeros",
	    getHelperText: (error) =>
            error ? "El nombre debe ir en mayuscula" : "No debe llevar numeros",
	    isValid: (value) => /^[A-Z\s]*$/.test(value),
	},
	{
	    id: "Nombre2",
	    label: "Segundo Nombre",
	    placeholder: "JUAN",
	    value: "",
	    error: false,
      required: false,
	    select: false,
	    helperText: "No debe llevar numeros",
	    getHelperText: (error) =>
            error ? "El nombre debe ir en mayuscula" : "No debe llevar numeros",
	    isValid: (value) => /^[A-Z\s]*$/.test(value),
	},
	{
	    id: "Apellido1",
	    label: "Primer Apellido",
	    placeholder: "SALAZAR",
	    value: "",
	    error: false,
	    required: true,
	    select: false,
	    helperText: "No debe llevar numeros",
	    getHelperText: (error) =>
            error ? "El nombre debe ir en mayuscula" : "No debe llevar numeros",
	    isValid: (value) => /^[A-Z\s]*$/.test(value),
	},
	{
	    id: "Apellido2",
	    label: "Segundo Apellido",
	    placeholder: "HERRERA",
	    value: "",
	    error: false,
	    required: true,
	    select: false,
	    helperText: "No debe llevar numeros",
	    getHelperText: (error) =>
            error ? "El nombre debe ir en mayuscula" : "No debe llevar numeros",
	    isValid: (value) => {
		return /^[A-Z\s]*$/.test(value);
	    },
	},
	
	{
	    id: "email",
	    label: "Email",
	    placeholder: "nombre.apellido@email.com",
	    value: "",
	    error: false,
	    readOnly: true,
	    select: false,
	    helperText: "",
	    getHelperText: (error) => (error ? "Formato invalido" : ""),
	    isValid: (value) =>
		/^([a-zA-Z]*)*\.*([a-zA-Z0-9]*)*@*([a-zA-Z0-9_\-]*)*\.{0,1}([a-zA-Z]{2,5})*$/.test(
		    value
		),
	},
	{
	    id: "TipoDocumento",
	    label: "Tipo documento",
	    placeholder: "",
	    value: "",
	    select: true,
	    error: false,
	    readOnly: false,
	    helperText: "Selecciona tipo de documento  ",
	    getHelperText: (error) => (error ? "Formato invalido" : ""),
	    isValid: (value) => true,
	},
    ]);
    

    const onChange = ({ target: { id, value } }) => {
	const newInputs = [...inputs];
	const index = inputs.findIndex((input) => input.id === id);
	const input = inputs[index];
	const isValid = input.isValid(value);
	console.log(value);
	newInputs[index] = {
	    ...input,
	    value: value,
	    error: !isValid,
	    helperText: input.getHelperText(!isValid),
	};
	setInputs(newInputs);
    };
    

    const onSubmit = (e) => {
	e.preventDefault();
	console.log(
	    "submit",
	    inputs.map((items) => items.value)
	);
    };


    return (
	<>
	    <form onSubmit={onSubmit}>

		<Typography variant="h4" className={classes.margin}>
		    Datos personales
		</Typography>

		<Grid container spacing={8} xs={9} className={classes.container}>

		    {inputs.map((input) => (

			<Grid item key={input.id}>

			    <TextField
				id={input.id}
				label={input.label}
				placeholder={input.placeholder}
				helperText={input.helperText}
				value={input.value}
				onChange={onChange}
				error={input.error}
				variant="filled"
				select={input.select}
				required={input.required}
				InputProps={{ readOnly: input.readOnly || false }}
			    >

				{input.select &&
				 optIdent.map((option) => (
				     <MenuItem key={option.value} value={option.value}>
					 {option.label}
				     </MenuItem>
				 ))}

			    </TextField>

			</Grid>

		    ))}
		</Grid>

		<Typography className={classes.margin} variant="h4">
		    Datos personales
		</Typography>

		<Button
		    type="submit"
		    variant="contained"
		    color="primary"
		    className={classes.margin}
		>
		    Agregar
		</Button>

	    </form>
	</>
    );
};

export default AddUser;
