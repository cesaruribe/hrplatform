import React, { useState, useEffect } from "react";
import { DataGrid, GridToolbar } from "@material-ui/data-grid";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import clsx from "clsx";

const headers = [
    {
	// Button configuration
	field: " ",
	width: 120,
	sortable: false,
	headerName: "Acciones",
	disableClickEventBubbling: true,
	headerClassName: "super-app-theme--header",
	renderCell: (params) => {
	    return (
		<>
		    <IconButton aria-label="delete" size="small">
			<DeleteIcon />
		    </IconButton>
		    <IconButton aria-label="delete" size="small">
			<EditIcon />
		    </IconButton>
		</>
	    );
	},
    },
    {
	field: "Apellido1",
	headerName: "Primer Apellido",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Apellido2",
	headerName: "Segundo Apellido",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Nombre1",
	headerName: "Primer Nombre",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Nombre2",
	headerName: "Segundo Nombre",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Pais",
	headerName: "Pais",
	flex: 1,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "TipoDocumento",
	headerName: "Tipo de documento",
	width: 150,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Identificacion",
	headerName: "Identificacion",
	width: 150,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "email",
	headerName: "Email",
	width: 300,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "FechaCreacion",
	headerName: "Fecha de Ingreso",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Area",
	headerName: "Area",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
    {
	field: "Estado",
	headerName: "Estado",
	flex: 2,
	headerClassName: "super-app-theme--header",
	cellClassName: (params) =>
	clsx("super-app", {
            negative: params.value === "Inactivo",
            positive: params.value === "Activo",
	}),
    },
    {
	field: "FechaRegistro",
	headerName: "Fecha de registro",
	width: 200,
	headerClassName: "super-app-theme--header",
    },
];

// Get data from the server
const getList = () => {
    return fetch("http://localhost:8000/users/").then((data) => data.json());
};

const useStyles = makeStyles({
    root: {
	"& .super-app-theme--header": {
	    backgroundColor: "rgb(100 117 206)",
	    color: "#fff",
	},
	backgroundColor: "#fff",
	"& .super-app.negative": {
	    backgroundColor: "#d47483",
	    color: "#1a3e72",
	    fontWeight: "600",
	},
	"& .super-app.positive": {
	    backgroundColor: "rgba(157, 255, 118, 0.49)",
	    color: "#1a3e72",
	    fontWeight: "600",
	},
    },
});

const AllUsers = () => {
    const classes = useStyles();
    const [users, setUsers] = useState([]);
    
    useEffect(() => {
	let isMounted = true;
	getList().then((items) => {
	    if (isMounted) {
		setUsers(items);
	    }
	});
	return () => (isMounted = false);
    }, []);
    
  return (
      <div
	  style={{
              height: "85vh",
              width: "90%",
              margin: "20px auto",
              padding: 20,
	  }}
	  className={classes.root}
      >
	  <DataGrid
              rows={users}
              columns={headers}
              components={{
		  Toolbar: GridToolbar,
              }}
	  />
      </div>
  );
};

export default AllUsers;
