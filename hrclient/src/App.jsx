import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import React, { Fragment, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import AllUsers from "./components/Users/AllUsers";
import AddUser from "./components/Users/AddUser";
import useToken from "./hooks/useToken";
import Navbar from "./components/NavBar/Navbar";


const estilos = makeStyles((theme) => ({
    toolbar: theme.mixins.toolbar,
}));


function App() {
    
    const classes = estilos();
    const { token, setToken } = useToken({});
    
    if (!token) {
	return (
	    <Fragment>
		<h1>Not logged</h1>
		<Button
		    color="primary"
		    variant="default"
		    onClick={() => setToken({ token: true })}
		>
		    Login
		</Button>
	    </Fragment>
	);
    }
    
    const logout = () => setToken({ token: false });
    
    return (
	<div style={{ backgroundColor: "#eee", height: "100vh" }}>
	    <Router>
		<Navbar logout={logout} />
		<div className={classes.toolbar} />
		
		<Switch>
		    <Route exact path="/users" component={AllUsers} />
		    <Route path="/users/edit/" component={AddUser} />
		    <Route path="/" exact>
			<h1>Main Page</h1>
		    </Route>
		</Switch>
	    </Router>
	</div>
    );
}

export default App;
